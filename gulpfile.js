var gulp = require('gulp'),
    Promise = require('es6-promise').Promise,
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    uglify = require('gulp-uglifyjs'),
    jsmin = require('gulp-jsmin'),
    rename = require('gulp-rename'),
    rigger = require('gulp-rigger');

gulp.task('sass', function () {
    gulp.src('public/stylesheets/main.scss')
        .pipe(sass())
		.pipe(autoprefixer({
			browsers: ["> 0%"],
			cascade: false
		}))
		.pipe(cssmin())
		.pipe(rename({
			basename: "maslaw",
			extname: ".css"
		}))
        .pipe(gulp.dest('public/css'));
});

gulp.task('sass:watch', function () {
	gulp.watch('public/stylesheets/**/*.scss', ['sass']);
});

gulp.task('default', ['sass']);
