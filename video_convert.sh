#!/usr/bin/env bash

#    0        1                         2                                  3                         4                                   5                                 6
# video.sh -source /home/dev/www/8march/public/uploads/raw_video/ -output_video_dir /home/dev/www/8march/public/uploads/video/ -output_screen_dir /home/dev/www/8march/public/uploads/previews/

if [ -z "$2" ]
then
    exit "Specify source!"
fi
while true;
do
    if [ "$(ls -A $2)" ]; then
        for i in $2*
        do
            date +%Y-%m-%d_%H-%M-%S >> log.log
            echo "\n" >> lo
            echo  "$i processing" >> log.log
            b=$(basename $i)
            ffmpeg -y -i $i -async 1 -metadata:s:v:0 start_time=0 -s 1280x720 -threads 1 -vcodec libx264 -acodec aac -b:v 1000k -refs 6 -coder 1 -sc_threshold 40 -flags +loop -me_range 16 -subq 7 -i_qfactor 0.71 -qcomp 0.6 -qdiff 4 -trellis 1 -b:a 64k $4$b
            ffmpeg -i $i -vframes 1 -f image2 $6$b.jpg
            rm -f $i
            echo  "\n $i ended " >> log.log
        done
        sleep 300 # 5 minutes sleep
    fi
    sleep 60 # 1 minute sleep
done
