<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Chat;

require dirname( dirname( dirname(__DIR__) ) ) . '/vendor/autoload.php';
require dirname( dirname(__DIR__) ) . '/Events/WsEvent.php';

class StartWs extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'my:StartWs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run my command.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat()
                )
            ),
            9000
        );

        $server->run();

        $this->info('Server is fired up on port: 9000');
    }

}