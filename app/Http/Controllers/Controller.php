<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

require dirname(__DIR__) . '/ManagerModel.php';

class Controller extends BaseController {

	private $model;

	public function __construct() {

		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		$this->model = new \App\ManagerModel;
	}

	public function index() {

		if( !isset( $_SESSION['user'] ) || !isset( $_SESSION['user']['athorized'] ) ) {

			return view('login');
		} else {

			return view( 'index', [
                'images' => $this->model->prepareImagesArray( $this->model->getRecentImages() ),
                'video'  => $this->model->getVideo()
            ] );
		}
	}

	public function login(Request $request) {
		$gender = $request->input('gender');
		$login = $request->input('login');
		$password = $request->input('password');

		$result = $this->model->checkLogin( $login, $password, $gender );

		return response()->json([
			'result'  => $result,
		]);
	}

	public function logout() {
		session_destroy();

		foreach ( $_COOKIE as $key => $value ) {
			if ( $key != 'PHPSESSID' ) {
				setcookie( $key, "", time() - 1800, '/' );
			}
		}

		return redirect('/');
	}

	public function parseInstagram() {
        $parser = dirname( __FILE__ ) . '/phantom-parse.js';

        $output = shell_exec( 'phantomjs --debug=false --ignore-ssl-errors=true --ssl-protocol=any ' . $parser );

        $image_urls = explode( ',', trim( $output ) );

        $status = $this->model->addNewImages( $image_urls );

        return response()->json( [
            'status'  => $status,
        ] );
    }

	public function instagram() {

        $raw_data = \DB::select( 'SELECT DISTINCT * FROM images WHERE id > ' . $_SESSION['user'][ 'last_image' ] .' ORDER BY id ASC LIMIT 0,20' );
        $images = $this->model->prepareImagesArray( $raw_data );

        return response()->json( [
            'result'  => $images,
        ] );
	}

    public function upload( Request $request ) {

        if( $request->hasFile( 'video-upload' )
            && $request->file( 'video-upload' )->isValid()
            && 200 >= $request->file( 'video-upload' )->getClientSize() / ( 1024.0 * 1024.0 ) ) {

            $request->file( 'video-upload' )->move( base_path() . '/public/video', $request->file( 'video-upload' )->getClientOriginalName() );
        }
    }

    public function getVideo() {

        return response()->json( [
            'result' => $this->model->getVideo()
        ] );
    }

    public function getLoginName() {


        return response()->json( [

            'result' => $_SESSION['user'][ 'login' ],
            'gender' => $_SESSION['user'][ 'gender' ]
        ] );
    }
}
