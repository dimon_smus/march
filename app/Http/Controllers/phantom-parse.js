var webPage = require('webpage'),
    page = webPage.create(),
    url = 'https://www.instagram.com/explore/tags/nixkiller/';

page.open( url, function ( status ) {

    var elements = page.evaluate( function() {
        var imageUrls = [],
            imageElements = document.getElementsByClassName( '_icyx7' );

        for( var i = 0; i < imageElements.length; ++i ) {

            imageUrls.push( imageElements[ i ].src );
        }

        return imageUrls;
    } );

    console.log( elements );

    phantom.exit();
} );