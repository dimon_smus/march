<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'Controller@index');

$app->post( '/login', 'Controller@login');
$app->get( '/logout', 'Controller@logout');
$app->post( '/instagram', 'Controller@instagram');
$app->post( '/upload', 'Controller@upload');
$app->post( '/get_video', 'Controller@getVideo');
$app->post( '/get_login', 'Controller@getLoginName');
$app->get( '/instagram_parse', 'Controller@parseInstagram');
