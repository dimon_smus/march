<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerModel extends Model {

	public function checkLogin( $login, $password , $gender ) {

		if( !isset( $_SESSION['user'] ) || !isset( $_SESSION['user']['athorized'] ) ) {
			$connection = ldap_connect( '10.10.0.7' );

			ldap_set_option($connection, LDAP_OPT_REFERRALS, 0);
			ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);

			if( $bind = @ldap_bind( $connection, $login . '@nixsolutions.com', $password ) ) {
				$search = ldap_search( $connection, 'OU=Sites,DC=nixsolutions,DC=com', "(&(objectClass=user)(samaccountname=". $login ."))" );

				$entries = ldap_get_entries( $connection, $search );
                $last_image = $this->getRecentImages( 1 );

				$_SESSION['user'] = [
					'athorized'  => true,
					'login'      => $entries[ 0 ][ 'displayname' ][ 0 ],
					'password'   => $password,
					'gender'     => $gender . rand( 1, 8 ),
                    'last_image' => $last_image[ 0 ]->id
				];

				SetCookie( "login", $entries[ 0 ][ 'displayname' ][ 0 ] );
				SetCookie( "gender", $gender . rand( 1, 8 ) );

				return true;
			} else {

				return false;
			}
		} else {

			return false;
		}
	}

	public function addNewImages( $images = [] ) {

        if( !sizeof( $images ) ) { return true; }

        foreach ( $images as $key => $image ) {

            $images[ $key ] = ( '("' . $image . '")' );
        }

        $stringImages = implode( ',', $images );

        $result = \DB::insert( 'INSERT INTO images ( path ) VALUES ' . $stringImages . ' ON DUPLICATE KEY UPDATE `path`=VALUES(`path`);' );

        return $result;
    }

    public function prepareImagesArray( $raw_data ) {
        $images = [];

        foreach ( $raw_data as $element ) {

            array_push( $images, $element->path );
        }

        return $images;
    }

    public function getRecentImages( $limit = 20 ) {

        return \DB::select( 'SELECT DISTINCT * FROM images ORDER BY id DESC LIMIT 0,' . $limit );
    }

    public function getVideo() {
        $video = [];

        $iter = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator( base_path() . '/public/video_ready', \RecursiveDirectoryIterator::SKIP_DOTS ),
            \RecursiveIteratorIterator::SELF_FIRST,
            \RecursiveIteratorIterator::CATCH_GET_CHILD
        );

        foreach ( $iter as $path => $dir ) {
            if ( ! $dir->isDir() && strpos( $path, '.mp4' ) ) {
                $appliedPath = str_replace( dirname( base_path() . '/public' ), '', $path );

                array_push( $video, str_replace( '.mp4', '', basename( $appliedPath ) ) );
            }
        }

        return $video;
    }

}
