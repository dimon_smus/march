
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="author" name="Oleg Maslov">
	<title>8 марта</title>
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon2017/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicon2017/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon2017/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon2017/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon2017/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon2017/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon2017/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon2017/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon2017/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicon2017/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/favicon2017/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicon2017/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="images/favicon2017/manifest.json">
	<link rel="mask-icon" href="images/favicon2017/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="images/favicon2017/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="/css/maslaw.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="/js/plugins/jquery-1.12.0.min.js"></script>
	<script src="/js/login-manager.js"></script>

</head>
<body>
<div class="login-wrapper">
	<section class="login">
		<div class="vertical-align">
			<h3>Авторизация</h3>
			<p class="error-login-msg error-msg">Неверный логин или пароль</p>
			<p class="error-gender-msg error-msg">Выберите пол</p>
			<form action="/login" method="post">
				<input type="text" name="login" placeholder="Доменный логин"  autocomplete="off">
				<input type="password" name="password" placeholder="Доменный пароль"  autocomplete="off">
				<input type="radio" name="gender" value="male" id="male-type"><label for="male-type"></label>
				<input type="radio" name="gender" value="female" id="female-type"><label for="female-type"></label>

				<button type="submit">Войти</button>
			</form>
		</div>
	</section>

</div>
</body>
</html>
