<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="author" name="Yevhen Kotelnytskyi, Smus Dimon, Maslov Oleg, Maximiuse and others best of men">
	<title>#nix8march2016</title>
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.html">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.html">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.html">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.html">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.html">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.html">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.html">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.html">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.html">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.html">
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.html">
	<link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.html">
	<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.html">
	<link rel="manifest" href="images/favicon/manifest.html">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.html">
	<meta name="theme-color" content="#ffffff">
	<link href="css/style.css" rel="stylesheet">	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="css/plugins/jquery.fullPage.css">
	<link rel="stylesheet" href="css/plugins/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" type="text/css" href="css/plugins/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/plugins/slick-theme.css"/>
	<script src="js/plugins/jquery-1.12.0.min.js"></script>
	<script src="js/plugins/jquery.cookie.js"></script>
	<script src="js/plugins/modernizr.custom.js"></script>
	<script src="js/plugins/jquery.fullPage.min.js"></script>
	<script src="js/plugins/jquery.mCustomScrollbar.min.js"></script>
	<script src="js/plugins/slick.min.js"></script>
	<script src="js/jwplayer/jwplayer.js"></script>
	<script>jwplayer.key="BKhOFj9vM3uAhiPrpkIjK9s65KR7mUpfsKGmrA==";</script>
	<script src="js/frontEnd.js"></script>
	<script src="js/chat-manager.js"></script>
	<script src="js/instagram-manager.js"></script>
</head>
<!--
╔══╗──╔══╗
║╔═╝──║╔╗║
║║────║╚╝║
║║────║╔╗║
║╚═╗──║╚╝║
╚══╝──╚══╝
╔╗──╔╗╔══╗╔═══╗╔════╗╔══╗
║║──║║║╔╗║║╔═╗║╚═╗╔═╝║╔╗║
║╚╗╔╝║║╚╝║║╚═╝║──║║──║╚╝║
║╔╗╔╗║║╔╗║║╔══╝──║║──║╔╗║
║║╚╝║║║║║║║║─────║║──║║║║╔╗
╚╝──╚╝╚╝╚╝╚╝─────╚╝──╚╝╚╝╚╣
─╔══╗─╔══╗╔═══╗╔══╗╔══╗╔╗╔╗╔═══╗
─║╔╗║─║╔╗║║╔═╗║║╔╗║║╔═╝║║║║║╔══╝
─║║║║─║║║║║╚═╝║║║║║║║──║║║║║╚══╗
─║║║║─║║║║║╔══╝║║║║║║──║║╔║║╔══╝
╔╝╚╝╚╗║╚╝║║║───║╚╝║║║──║╚╝║║╚══╗
╚════╝╚══╝╚╝───╚══╝╚╝──╚══╝╚═══╝
╔╗╔╗╔╗╔═══╗╔╗╔╗╔╗╔╗╔╗╔╗╔╗╔╗╔╗╔╗──╔╗╔╗╔╗╔╗
║║║║║║║╔══╝║║║║║║║║║║║║║║║║║║║║──║║║║║║║║
║╚╝╚╝║║╚══╗║╚╝║║║║║║║║║║║║╚╝║║╚══╣║║║║║║║
║╔╗╔╗║║╔══╝║╔╗║║║║║║║║║╔║║╔╗║║╔═╗║║╚╝╚╝╚╝
║║║║║║║╚══╗║║║║║╚╝╚╝║║╚╝║║║║║║╚═╝║║╔╗╔╗╔╗
╚╝╚╝╚╝╚═══╝╚╝╚╝╚════╗╚══╝╚╝╚╝╚═══╩╝╚╝╚╝╚╝
-->
<body>
	<p class="mobile-msg"><span class="mobile-msg-icon"></span>Полная версия сайта доступна на PC</p>
	<a href="/logout" class="exit">Выход</a>
	<div class="wrapper">
		<div class="left-container">
			<section id="page-wrapper" class="full">

				<div class="section">
					<div class="radio on">
						<div class="radio-content">
							<strong>Радио</strong>
							Вкл/Выкл
						</div>
					</div>
					<div class="audio">
						<div id="audio-player"></div>
					</div>
					<div class="congratulation"></div>
				</div>

				<div class="section">
					<div class="video">
						<div id="video-player"></div>

					</div>
					<img src="/images/salute.png" alt="" class="salute" hidden>
					<div class="gift"></div>
					<div class="greeting-masters hidden">
						<div><img src="/images/greeting_masters/slider1.png"></div>
						<div><img src="/images/greeting_masters/slider2.png"></div>
						<div><img src="/images/greeting_masters/slider3.png"></div>
						<div><img src="/images/greeting_masters/slider4.png"></div>
						<div><img src="/images/greeting_masters/slider5.png"></div>
						<div><img src="/images/greeting_masters/slider6.png"></div>
						<div><img src="/images/greeting_masters/slider7.png"></div>
						<div><img src="/images/greeting_masters/slider8.png"></div>
						<div><img src="/images/greeting_masters/slider9.png"></div>
						<div><img src="/images/greeting_masters/slider10.png"></div>
					</div>
				</div>

			</section>
		</div>
		<div class="right-container">
			<div class="chat-wrapper">
				<ul>
					
				</ul>
			</div>
			<div class="message-container">
				<form class="chat-form" action="#">
					<p>
						<textarea name="" id="message" placeholder="Ваше сообщение..."></textarea>
						<input type="submit" value="">
					</p>
				</form>
			</div>
			<p class="insta-msg">
				Выкладывай фото с тегом <a href="https://www.instagram.com/explore/tags/nix8march2016/" target="_blank">#nix8march2016</a> в Instagram!
				<span>Профиль Instagram должен быть <a href="http://8marta.nixsolutions.com/open-insta.html" target="_blank">открытым.</a></span>
			</p>
		</div>
	</div>
	<form class="ping" action=""></form>
</body>
</html>