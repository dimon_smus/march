<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>8 march</title>
	<link rel="author" name="Oleg Maslov">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon2017/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicon2017/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon2017/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon2017/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon2017/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon2017/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon2017/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon2017/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon2017/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicon2017/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/favicon2017/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicon2017/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="images/favicon2017/manifest.json">
	<link rel="mask-icon" href="images/favicon2017/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="images/favicon2017/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="/css/maslaw.css">

	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

	<link rel="stylesheet" href="/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="/css/plugins/jquery.mCustomScrollbar.css">

	<script src="js/jquery.fancybox.min.js"></script>
	<script src="js/plugins/jquery.cookie.js"></script>
	<script src="js/plugins/modernizr.custom.js"></script>
	<script src="js/plugins/jquery.mCustomScrollbar.min.js"></script>
	<script src="js/jwplayer/jwplayer.js"></script>
	<script>jwplayer.key="BKhOFj9vM3uAhiPrpkIjK9s65KR7mUpfsKGmrA==";</script>

	<script src="js/ghost-layout.js"></script>
	<script src="js/chat-manager.js"></script>
	<script src="js/frontEnd.js"></script>
	<script src="js/video-manager.js"></script>
	<script src="js/instagram-manager.js"></script>
</head>

<body>
<div class="chat">
	<aside></aside>
	<section class="radio">

		<div class="radio__logo">
			<img src="images/radio-logo.png" alt="radio logo">
		</div>

		<div class="radio__controll">
			<label class="tgl tgl-gray">
				OFF
				<input type="checkbox" class="on" checked/>
				<span></span>
				ON
			</label>
		</div>

		<div id="audio-player"></div>
	</section>

	<div class="chat-wrapper">
		<ul>

		</ul>

		<div class="message-container">
			<form class="chat-form" action="#">
				<p>
					<textarea name="" id="message" placeholder=""></textarea>

					<span>
						Выкладывай фото с тегом<br />
						<span>#nix8march2017</span> в Instagram!
						Профиль Instagram должен быть <a href="open-insta.html" target="_blank">открытым</a>.
					</span>

					<input type="submit" value="Отправить">
				</p>
			</form>
		</div>
	</div>

</div>

<main class="main">

	<!-- <div class="page-view home-container">
		<aside></aside>

		<div class="page-view__content"> -->

			<!-- <section class="heart"></section> -->
			<!-- <iframe src="http://your.perfect.day.nixsolutions.com/3333index_full.html#table" width="100%" height="100%"></iframe>
		</div>
	</div> -->

	<div class="page-view translation-container">
		<aside></aside>

		<div class="page-view__content">

			<div class="translation-container__video">

				<p class="video__not-support">
					К сожалению, трансляция недоступна на мобильных устройствах.
				</p>

				<div id="video-player"></div>
			</div>
		</div>
	</div>

	<div class="page-view instagram-container">
		<aside><p class="counter">12</p></aside>

		<p class="page-view__message">
			Выкладывай фото с тегом	<span>#nix8march2017</span> в Instagram!
			Профиль Instagram должен быть <a href="open-insta.html" target="_blank">открытым</a>.
		</p>

		<div class="page-view__content">

			<div class="instagram-grid">

				<?php foreach( $images as $key => $image ) : ?>

					<div class="instagram-grid__item">
						<a class="fancybox" data-fancybox="group" rel="group" href="<?php echo $image; ?>">

							<img src="<?php echo $image; ?>" alt="img<?php echo $key; ?>" />
						</a>
					</div>

				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="page-view video-container">
		<aside><p class="counter">999</p></aside>

		<form class="upload-video-form" method="post" enctype="multipart/form-data" action="/upload">
			<span id="upload-video-path" class="upload-title">Путь к файлу...</span>
			<label for="video-upload" class="icon-folder">
				+
				<input type="file" id="video-upload" name="video-upload">
			</label>

			<input type="submit" id="submit" value="Отправить" />
			<input type="hidden" name="max_file_size" value="200" />
		</form>

		<div class="page-view__content">

			<div class="instagram-grid">

				<?php foreach( $video as $key => $item ) : ?>

				<div class="instagram-grid__item">
					<a class="fancybox" data-fancybox="video-group" rel="video-group" href="/video_ready/<?php echo $item; ?>.mp4" data-item="<?php echo $item; ?>">

						<img src="/video_ready/<?php echo $item; ?>.jpg" alt="img<?php echo $key; ?>" />
					</a>
				</div>

				<?php endforeach; ?>

			</div>
		</div>
	</div>

</main>

</body>
</html>
